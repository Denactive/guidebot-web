from django.contrib.auth import authenticate

from rest_framework import serializers
from rest_framework import status

from .models import User

# https://www.django-rest-framework.org/api-guide/serializers/#declaring-serializers

# поля из validated_data отсутствуют в data, если не прописаны
# в начале класса 

class RegistrationSerializer(serializers.ModelSerializer):
  """ Сериализация регистрации пользователя и создания нового. """

  # TODO: аватар урл в сериалайзере на получения пользователя
  # url = serializers.CharField(source='get_absolute_url', read_only=True)

  # Убедитесь, что пароль содержит не менее 8 символов, не более 128,
  # и так же что он не может быть прочитан клиентской стороной
  password = serializers.CharField(
    max_length=128,
    min_length=8,
    write_only=True
  )

  # заменю username на login.
  # В запросе теперь нужно поле login!
  # Во всех хендлерах data будет содержать поле username 
  login = serializers.CharField(source='username')
  # Клиентская сторона не должна иметь возможность отправлять токен вместе с
  # запросом на регистрацию. Сделаем его доступным только на чтение.
  token = serializers.CharField(max_length=255, read_only=True)

  class Meta:
    model = User
    # Перечислить все поля, которые могут быть включены в запрос
    # или ответ, включая поля, явно указанные выше.
    fields = ['email', 'login', 'password', 'token']

  def create(self, validated_data):
    # Использовать метод create_user, который мы
    # написали ранее, для создания нового пользователя.
    return User.objects.create_user(**validated_data)

  # def update(self, instance, validated_data):
  #   instance.email = validated_data.get('email', instance.email)
  #   instance.username = validated_data.get('username', instance.login)
  #   instance.password = validated_data.get('password', instance.password)
  #   instance.save()
  #   return instance

  # Вызывается на is_valid() в вьюшке.
  # Unique constraint через login = serializers.CharField(source='username')
  # автоматически не проверяется, в отличие от использования чистого поля модели username
  def validate(self, data):
    user = data.get('username')
    record = User.objects.filter(username=user).first()

    if record:
      # username: user с таким username уже существует
      raise serializers.ValidationError(
        "Пользователь с таким логином уже существует.",
      )

    return data

class LoginSerializer(serializers.Serializer):
  # Здесь перечисляем те поля, которые по результатам валидации
  # попадут в итоговый json, т.е. это фильтр на поля
  # того словаря, который возвращает validate
  # по сравнивай 
  #   print(serializer.data)
  #   print(serializer.validated_data)
  # во views

  # Для аутентификации нужные логин и пароль
  login = serializers.CharField(
    source='username',
  )
  password = serializers.CharField(
    max_length=128, min_length=8, write_only=True,
  )
  # высылаем при успешной аутентификации
  token = serializers.CharField(
    max_length=255, read_only=True,
  )
  firstName = serializers.CharField(
    source='first_name', read_only=True,
  )
  lastName = serializers.CharField(
    source='last_name', read_only=True,
  )
  email = serializers.EmailField(
    read_only=True,
  )
  is_staff = serializers.BooleanField(
    read_only=True,
  )

  class Meta:
    model = User
    fields = ('login', 'email', 'firstName', 'lastName', 'password', 'is_staff')

  def validate(self, data):
    # В методе validate мы убеждаемся, что текущий экземпляр
    # LoginSerializer значение valid. В случае входа пользователя в систему
    # это означает подтверждение того, что присутствуют адрес электронной
    # почты и то, что эта комбинация соответствует одному из пользователей.
    login = data.get('username', None)
    password = data.get('password', None)

    # Вызвать исключение, если не предоставлена логин.
    if login is None and password is None:
      raise serializers.ValidationError(
        'Для входа нужны логин и пароль',
      )

    if login is None:
      raise serializers.ValidationError(
        # 'A login is required to log in.',
        'Для входа нужен логин',
      )

    # Вызвать исключение, если не предоставлен пароль.
    if password is None:
      raise serializers.ValidationError(
        # 'A password is required to log in.'
        'Для входа нужен пароль',
      )

    # Метод authenticate предоставляется Django и выполняет проверку, что
    # предоставленные почта и пароль соответствуют какому-то пользователю в
    # нашей базе данных. Мы передаем login как username, так как в модели
    # пользователя USERNAME_FIELD = username.
    user = authenticate(username=login, password=password)

    # Если пользователь с данными почтой/паролем не найден, то authenticate
    # вернет None. Возбудить исключение в таком случае.
    if user is None:
      raise serializers.ValidationError(
        'Неправильные логин и пароль',
        code=status.HTTP_404_NOT_FOUND,
      )

    # Django предоставляет флаг is_active для модели User. Его цель
    # сообщить, был ли пользователь деактивирован или заблокирован.
    # Проверить стоит, вызвать исключение в случае True.
    if not user.is_active:
      raise serializers.ValidationError(
        # 'This user has been deactivated.',
        'Аккаунт заблокирован',
        code=status.HTTP_403_FORBIDDEN,
      )

    # Метод validate должен возвращать словарь проверенных данных. Это
    # данные, которые передются в т.ч. в методы create и update, поле data сериалайзера.
    # в данном случае методы create и update стандартные, 
    userModel = User.objects.get(pk=user.pk)
    return userModel

class UserSerializer(serializers.ModelSerializer):
  """ Осуществляет сериализацию и десериализацию объектов User. """

  password = serializers.CharField(
    max_length=128, min_length=8, write_only=True
  )
  login = serializers.CharField(
    source='username', read_only=True,
  )
  email = serializers.EmailField(
    allow_blank=True,
  )
  firstName =  serializers.CharField(
    source='first_name', allow_blank=True,
  )
  lastName =  serializers.CharField(
    source='last_name', allow_blank=True,
  )
  is_staff = serializers.BooleanField(
    read_only=True,
  )

  class Meta:
    model = User
    fields = ('login', 'email', 'firstName', 'lastName', 'password', 'is_staff')

    # Параметр read_only_fields является альтернативой явному указанию поля
    # с помощью read_only = True, как мы это делали для пароля выше.
    # Причина, по которой мы хотим использовать здесь 'read_only_fields'
    # состоит в том, что нам не нужно ничего указывать о поле. В поле
    # пароля требуются свойства min_length и max_length,
    # но это не относится к полю токена.
    read_only_fields = ('is_staff',)

  def validate(self, data):
    print('[Validator]', data)
    # Пустые поля = ничего не меняем.
    if data.get('email', None) == '':
      data.pop('email')
    if data.get('first_name', None) == '':
      data.pop('first_name')
    if data.get('last_name', None) == '':
      data.pop('last_name')
    return data

  # вызывается в update методе вьюшки, где используется этот сериализатор
  # во время вызова serializer.save() 
  def update(self, instance, validated_data):
    """ Выполняет обновление User. """

    # В отличие от других полей, пароли не следует обрабатывать с помощью
    # setattr. Django предоставляет функцию, которая обрабатывает пароли
    # хешированием и 'солением'. Это означает, что нам нужно удалить поле
    # пароля из словаря 'validated_data' перед его использованием далее.
    password = validated_data.pop('password', None)

    for key, value in validated_data.items():
      # Для ключей, оставшихся в validated_data мы устанавливаем значения
      # в текущий экземпляр User по одному.
      setattr(instance, key, value)

    if password is not None:
      # 'set_password()' решает все вопросы, связанные с безопасностью
      # при обновлении пароля, потому нам не нужно беспокоиться об этом.
      instance.set_password(password)

    # После того, как все было обновлено, мы должны сохранить наш экземпляр
    # User. Стоит отметить, что set_password() не сохраняет модель.
    instance.save()

    return instance
