from django.shortcuts import render

from rest_framework import status
from rest_framework.generics import RetrieveAPIView, UpdateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.renderers import BrowsableAPIRenderer

from .renderers import UserJSONRenderer
from .serializers import (
    LoginSerializer, RegistrationSerializer, UserSerializer,
)

class RegistrationAPIView(APIView):
  """
  Разрешить всем пользователям (аутентифицированным и нет) доступ к данному эндпоинту.
  """
  permission_classes = (AllowAny,)
  serializer_class = RegistrationSerializer
  # renderer_classes = [UserJSONRenderer, BrowsableAPIRenderer]

  def post(self, request):
    # Паттерн создания сериализатора, валидации и сохранения - довольно
    # стандартный, и его можно часто увидеть в реальных проектах.
    serializer = self.serializer_class(data=request.data)
    # Return a 400 response if the data was invalid.
    serializer.is_valid(raise_exception=True)
    serializer.save()

    return Response(data=serializer.data, status=status.HTTP_201_CREATED)

class LoginAPIView(APIView):
  permission_classes = (AllowAny,)
  # renderer_classes = (UserJSONRenderer,)
  serializer_class = LoginSerializer

  def post(self, request):
    # Обратите внимание, что мы не вызываем метод save() сериализатора, как
    # делали это для регистрации. Дело в том, что в данном случае нам
    # нечего сохранять. Вместо этого, метод validate() делает все нужное.
    
    serializer = self.serializer_class(data=request.data)
    serializer.is_valid(raise_exception=True)

    print(serializer.data)
    print(serializer.validated_data)

    return Response(serializer.data, status=status.HTTP_200_OK)

class UserRetrieveAPIView(RetrieveAPIView):
  '''
  GET method only
  '''
  permission_classes = (IsAuthenticated,)
  # renderer_classes = (UserJSONRenderer,)
  serializer_class = UserSerializer

  def retrieve(self, request, *args, **kwargs):
    # Здесь нечего валидировать или сохранять. Мы просто хотим, чтобы
    # сериализатор обрабатывал преобразования объекта User во что-то, что
    # можно привести к json и вернуть клиенту.

    # Во всех запросах от авторизованных (IsAuthenticated) пользователей
    # в запросе добавлено поле user! см GuideBotServer/backends
    serializer = self.serializer_class(request.user)
    print('[View] [User]:', request.user)
    # print('[View] [User]:', serializer.data)
    # print(request.user.login)
    # print(request.user.email)
    # print(request.user.first_name)
    # print(request.user.last_name)

    return Response(serializer.data, status=status.HTTP_200_OK)


class UserUpdateAPIView(UpdateAPIView):
  '''
  PUT / PATCH methods
  '''
  permission_classes = (IsAuthenticated,)
  # renderer_classes = (UserJSONRenderer,)
  serializer_class = UserSerializer

  def update(self, request, *args, **kwargs):
    # serializer_data = request.data.get('user', {})
    serializer_data = request.data

    # Во всех запросах от авторизованных (IsAuthenticated) пользователей
    # в запросе добавлено поле user! см GuideBotServer/backends
    serializer = self.serializer_class(
        request.user, data=serializer_data, partial=True
        # request, data=serializer_data, partial=True
    )
    serializer.is_valid(raise_exception=True)
    serializer.save()

    return Response(serializer.data, status=status.HTTP_200_OK)
