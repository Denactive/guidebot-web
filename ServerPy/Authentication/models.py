import jwt
from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth.models import (
	AbstractBaseUser, BaseUserManager, PermissionsMixin
)
from django.core.validators import RegexValidator
from django.db import models

# JWT авторизация отлично описана здесь
# https://habr.com/ru/post/538040/



LoginValidator = RegexValidator(
  regex='^[a-zA-Z0-9_]{4,}$',
  message='Логин - это латинские буквы, цифры и \
    нижнее подчеркивание (_). Длина логина - от 4 символов',
  code='invalid_username',
)

FirstNameValidator = RegexValidator(
  regex='^\\w{2,}$',
  message='Имя должно состоять из 2 и более букв',
  code='invalid_first_name',
)

LastNameValidator = RegexValidator(
  regex='^[\\w]{1,}\'?-?[\\w]{1,}(\\s[\\w]{1,})?$',
  message='Фамилия должна состоять из 2 и более букв и может содержать 1 пробел, апостроф или дефис',
  code='invalid_last_name',
)

PasswordValidator = RegexValidator(
  regex='^\\S{8,128}$',
  message='Пароль - это комбинация непробельных символов длиной более 8',
  code='invalid_last_name',
)


class UserManager(BaseUserManager):
  
  # Django требует, чтобы кастомные пользователи определяли свой собственный
  # класс Manager. Унаследовавшись от BaseUserManager, мы получаем много того
  # же самого кода, который Django использовал для создания User (для демонстрации).
  # https://docs.djangoproject.com/en/3.1/topics/auth/customizing/#substituting-a-custom-user-model
  # https://docs.djangoproject.com/en/3.1/topics/db/managers/
  
  def create_user(self, username, email, first_name='', last_name='', password=None):
    """ Создает и возвращает пользователя с имэйлом, паролем и именем. """
    if username is None:
      raise TypeError('Все пользователи должны иметь уникальный логин.')

    if email is None:
      raise TypeError('Все пользователи должны иметь уникальный e-mail.')

    if first_name is None or last_name is None:
      raise TypeError('Пользователи не должны иметь имя None.')

    user = self.model(
      username=username,
      first_name=first_name,
      last_name=last_name,
      email=self.normalize_email(email),
    )
    user.set_password(password)
    user.save()

    return user

  def create_superuser(self, username, email,  first_name, last_name, password):
    """ Создает и возввращет пользователя с привилегиями суперадмина. """
    if password is None:
        raise TypeError('Superusers must have a password.')

    user = self.create_user(username, email, first_name, last_name, password)
    user.is_superuser = True
    user.is_staff = True
    user.save()

    return user

class User(AbstractBaseUser, PermissionsMixin):
  # Каждому пользователю нужен понятный человеку уникальный идентификатор,
  # который мы можем использовать для предоставления User в пользовательском
  # интерфейсе. Мы так же проиндексируем этот столбец в базе данных для
  # повышения скорости поиска в дальнейшем.
  
  # Используетсся для входы в систему
  username = models.CharField(db_index=True, max_length=255, unique=True, validators=[LoginValidator])

  @property
  def login(self):
    """ alias на username """
    return self.username

  email = models.EmailField(db_index=True, unique=True)
  first_name = models.CharField(blank=True, max_length=255, validators=[FirstNameValidator])
  last_name = models.CharField(blank=True, max_length=255, validators=[LastNameValidator])

  # Когда пользователь более не желает пользоваться нашей системой, он может
  # захотеть удалить свой аккаунт. Для нас это проблема, так как собираемые
  # нами данные очень ценны, и мы не хотим их удалять :) Мы просто предложим
  # пользователям способ деактивировать учетку вместо ее полного удаления.
  # Таким образом, они не будут отображаться на сайте, но мы все еще сможем
  # далее анализировать информацию.
  is_active = models.BooleanField(default=True)

  # Этот флаг определяет, кто может войти в административную часть нашего
  # сайта. Для большинства пользователей это флаг будет ложным.
  is_staff = models.BooleanField(default=False)

  # Встроенные функции Джанго
  # https://www.dothedev.com/blog/django-created-at-updated-at-auto-fields/
  # Временная метка создания и последнего обновления объекта.
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  # Дополнительный поля, необходимые Django
  # при указании кастомной модели пользователя.

  # Свойство USERNAME_FIELD сообщает нам, какое поле мы будем использовать
  # для входа в систему.
  USERNAME_FIELD = 'username'
  # A list of the field names that will be prompted for when creating
  # a user via the createsuperuser management command
  REQUIRED_FIELDS = ['email', 'first_name', 'last_name']

  # Сообщает Django, что определенный выше класс UserManager
  # должен управлять объектами этого типа.
  objects = UserManager()

  # https://docs.djangoproject.com/en/4.0/ref/models/options/
  class Meta:
    # для отображения в админке
    verbose_name = "пользователь"
    verbose_name_plural = "пользователи"

  def __str__(self):
    """ Строковое представление модели (отображается в консоли) """
    return "{}{}(@{})".format(
      self.first_name + ' ' if self.first_name != "" else "без имени ",
      self.last_name + ' ' if self.last_name != "" else "без фамилии " if self.first_name != "" else "",
      self.username,
    )

  @property
  def token(self):
    """
    Позволяет получить токен пользователя путем вызова user.token, вместо
    user._generate_jwt_token(). Декоратор @property выше делает это
    возможным. token называется "динамическим свойством".
    """
    return self._generate_jwt_token()

  def get_full_name(self):
    """
    Этот метод требуется Django для таких вещей, как обработка электронной
    почты. Обычно это имя фамилия пользователя, но поскольку мы не
    используем их, будем возвращать username.
    """
    return self.first_name + ' ' + self.last_name

  def get_short_name(self):
    """ Аналогично методу get_full_name(). """
    return self.first_name

  def _generate_jwt_token(self):
    """
    Генерирует веб-токен JSON, в котором хранится идентификатор этого
    пользователя, срок действия токена составляет 1 день от создания
    """
    dt = datetime.now() + timedelta(days=1)

    # https://stackoverflow.com/questions/33198428/jwt-module-object-has-no-attribute-encode 
    return jwt.encode({
      'id': self.pk,
      # https://stackoverflow.com/questions/54503213/invalid-format-string-at-generate-jwt-token
      'exp': dt.utcfromtimestamp(dt.timestamp())
    }, settings.SECRET_KEY, algorithm='HS256')
