# GuideBot
Открытая часть проекта автономного робота. Проект разрабатывается в МГТУ им Баумана. Django, Django-Channels, TypeScript, React, Redux

**[English](./README_ENG.md)**

# Материалы #
- [Документация и описание](https://drive.google.com/drive/folders/1zevx_SmwngV8QqQkeFI_AQbh50U1BMo9?usp=sharing)

# Научрук #
- [Антон Канев](http://iu5.bmstu.ru/user/profile.php?id=2271)

# Разработчики #
- [Кириллов Денис](https://vk.com/denactive)

# Установка и начало работы #

### Разработка
**dev-сервер + Django**

**Запуск одной кнопкой (из корня проекта)**

Скрипт под Windows 10
  1. ```npm i```
  2. ```npm start```

Скрипт под Linux Ubuntu 20.04
  1. ```sudo apt install tmux```
  2. ```sudo npm i```
  3. ```sudo npm start-linux```

Может, сделаю и кросс-платформенный скрипт.

**Админка**
``` http://localhost:8000/admin/ ```
Стандартная запись суперпользователя: Логин: ``` admin ```, Пароль: ``` admin ```

### Сервер
**Технические требования**

- Windows 10 / Ubuntu 20.04

**Программное обеспечение**

- python 3.9
- python libraries from [requirements.txt](./ServerPy/requirements.txt)

**Установка**

1. ```cd ServerPy```
2. ```sudo apt-get install python3.9```
3. __Необязательно__ В Linux интерпретатор питона именуется __python3__, а в  Windows - __py__.

    ```sudo alias py=python3```
4. __Необязательно__ Этот шаг можно пропустить, если не хотите использовать virtual environment. Переходите на пункт 6.

    ```sudo apt-get install python3.9-venv```
5. ```sudo py -m venv venv```
6. Windows: ```./venv/Scripts/activate```
    
    Linux: ```source venv/bin/activate```
7. ```sudo py -m pip install -r requirements```
8. ```sudo py manage.py collectstatic``` 
9. ```sudo py manage.py runserver 8080``` 

---

### Веб-клиент
**Технические требования**

- Windows 10 / Ubuntu 20.04

**Программное обеспечение**

- npm 6.14.11+
- node 14.16.0+
- js libraries from [package.json](./WebClient/package.json)

**Установка**

1. ```cd WebClient```
2. ```sudo curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh```
3. ```sudo bash nodesource_setup.sh``` - этот скрипт обновит данные о том, откуда можно скачать последнюю версию node
4. ```sudo rm nodesource_setup.sh``` - удалите установочный скрипт
5. ```sudo apt-get install nodejs npm```
6. ```sudo npm install -g npm@latest```
7. ```node -v``` - если все сделано правильно, версия node будет 14.16.0+
8. ```sudo npm i```
9. ```sudo npm start```

---

# Лицензия #
[Лицензия GNU GPLv2](./LICENSE)
