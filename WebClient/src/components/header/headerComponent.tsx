import { FC } from "react";
import { Link } from "react-router-dom";

import logo from '../../static/logo.svg';
import './header.scss';

type HeaderMapNameT<N extends string = string> = N;

type HeaderNameLinkMapT<N extends string = string, L extends string = string> = {
  [key in HeaderMapNameT<N>]: {
    headerSign: string,
    link: L,
  };
};

interface HeaderProps<N extends string = string, L extends string = string> {
  readonly nameLinkMapT: HeaderNameLinkMapT<N, L>,
  readonly displayedNames: N[],
  readonly nameOfActive: N;
}


/**
* Универсальный компонент хедера.
* Настраивается через конфиг.
* @param headerLinksMap - карта соответствия: {
*   headerSign: string;
*   link: L;
* }
* @param displayedNames - массив тех ключей карты, которые будут отображаться
* @param nameOfActive - имя того ключа, который подствечивается как текущая страница
*/
const HeaderComponent: FC<HeaderProps> = ({nameLinkMapT, displayedNames, nameOfActive}) => (
  <nav>
    <div className="nav-wrapper theme-color">
      <a href="/" className="logo left">
        <img className="material-icons" alt='Главная' src={logo} height={56}/>
      </a>
      <ul id="nav-mobile" className="left hide-on-med-and-down">
        {displayedNames.map((page, i) => (
          <li className={page === nameOfActive ? 'active' : ''} key={i}>
            <Link to={nameLinkMapT[page].link}>{nameLinkMapT[page].headerSign}</Link>
          </li>
        ))}
      </ul>
    </div>
  </nav>
);

export default HeaderComponent;
