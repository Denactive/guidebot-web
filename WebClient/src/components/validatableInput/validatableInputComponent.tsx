import { ChangeEvent, ChangeEventHandler, FC, FocusEvent, FocusEventHandler, FormEvent, InputHTMLAttributes, useState } from "react";

import './validatableInput.scss';

type ConditionT = {
  errorMsg: string,
  errorCondition: (input: string) => boolean,
}

interface ValidatableInputPropsT extends InputHTMLAttributes<HTMLInputElement> {
  errorConditions: ConditionT[],
}

const ValidatableInputComponent: FC<ValidatableInputPropsT> = ({errorConditions, ...rest}) => {
  const [value, setValue] = useState("");
  const [isOnceBlured, setIsOnceBlured] = useState(false);
  const [isValid, setIsValid] = useState(true);
  const [falseCondMsgs, setFalseCondMsgs] = useState([] as string[]);

  // в React onChange и onInput - одно и то же
  // https://stackoverflow.com/questions/38256332/in-react-whats-the-difference-between-onchange-and-oninput
  const originalOnChange = rest.onChange || (() => {}); 
  // onInput тупо не отслеживаем
  // const originalOnInput = rest.onInput || (() => {});

  const handleChange: ChangeEventHandler<HTMLInputElement> = (e: FormEvent<HTMLInputElement> | ChangeEvent<HTMLInputElement>) => {
    const valueLocal = e.currentTarget.value.trim();
    let isValidLocal = true;
    const falseCondMsgsLocal: string[] = [];

    errorConditions.forEach(({errorCondition, errorMsg}) => {
      if (errorCondition(valueLocal)) {
        falseCondMsgsLocal.push(errorMsg);
      }
    })

    if (valueLocal !== "" && falseCondMsgsLocal.length > 0) {
      isValidLocal = false;
    }
    setValue(valueLocal);
    setIsValid(isValidLocal)
    setFalseCondMsgs(falseCondMsgsLocal)

    originalOnChange(e as ChangeEvent<HTMLInputElement>);
  };

  // const handleInput = (e: FormEvent<HTMLInputElement>) => {
  //   console.log('input:', e.currentTarget.value)
  //   // originalOnInput(e);
  // }

  const handleBlur: FocusEventHandler<HTMLInputElement> = (e: FocusEvent<HTMLInputElement>) => {
      console.log('blur')
      setIsOnceBlured(true)
      // originalOnInput(e);
  }
  return (
    <div className="input-field col s12">
      <input className="validate"
        {...rest}
        onChange={handleChange}
        // onInput={handleInput}
        onBlur={handleBlur}
        value={value}
      >
      </input>
      <label htmlFor="test">test</label>
      <span className="helper-text"
        // data-error={regexp.password.errorMsg}
        // data-success="right" 
      >
      </span>
      {!isOnceBlured ? <p>hidden</p> : (
        <>
          <p>{Math.floor(falseCondMsgs.length * 100 / errorConditions.length)}% -&gt; {`${isValid}`}</p>
          {value === '' ? '' : falseCondMsgs.map((msg, idx) => (
            <p key={idx}>{msg}</p>
          ))}
        </>
        )
      }
    </div>
  );
};

export default ValidatableInputComponent;
