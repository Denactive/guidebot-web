import MainPage from '../pages/mainPage';
import AboutPage from '../pages/aboutPage';
import AuthPage from '../pages/authPage';

import { RouterMapT } from '../components/router/routerTypes';
import type {NameLinksMapT} from './appRoutingHeaderConfig';


// //////////////////////////////////////////////////////
//
//              Конфиг приложения: paths
//
// //////////////////////////////////////////////////////


const AppRoutingMap: RouterMapT<NameLinksMapT> = {
  'Main': {
    link: '/' ,
    Component: MainPage,
    isAuthRequired: true ,
  } ,
  'About': {
    link: '/about' ,
    Component: AboutPage,
    isAuthRequired: false ,
  } ,
  'Auth': {
    link: '/login' ,
    Component: AuthPage,
    isAuthRequired: false ,
  } ,
} as const;

export default AppRoutingMap;
