import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootStateT } from '../store';

export interface UserI {
  login: string,
  firstName: string,
  lastName: string,
  email: string,
}

interface AuthStateI extends UserI {
  isAuthorized: boolean,
  token: string,
}

interface ApiResponseLoginI {
  token: string,
  login: string,
  firstName?: string,
  lastName?: string,
  email?: string,
}

export interface ApiResponseUpdateI {
  login?: string,
  firstName?: string,
  lastName?: string,
  email?: string,
}

const initialState: AuthStateI = {
  isAuthorized: false,
  token: "",
  login: "",
  firstName: "",
  lastName: "",
  email: "",
}

const updateUser = (state: AuthStateI, payload: ApiResponseUpdateI) => {
  // for (const key in payload) {
  //   state[key] = payload[key] || ""
  // }
  state.login = payload.login || "";
  state.email = payload.email || "";
  state.firstName = payload.firstName || "";
  state.lastName = payload.lastName || "";
};

export const AuthSlice = createSlice({
  name: 'authentification',
  initialState,
  reducers: {
    logout: (state) => {
      state = Object.assign({}, initialState);
    },
  
    login: (state, action: PayloadAction<ApiResponseLoginI>) => {
      state.isAuthorized = true;
      state.login = action.payload.login;
      state.token = action.payload.token;
      updateUser(state, action.payload);
    },

    update: (state, action: PayloadAction<ApiResponseUpdateI>) => {
      updateUser(state, action.payload);
    }
  }
})

export const AuthActions = AuthSlice.actions;
export const selectAuth = (state: RootStateT) => state.AuthReducer;

export default AuthSlice.reducer
