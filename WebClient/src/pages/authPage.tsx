import { FC, useState, FormEvent, FormEventHandler, ChangeEventHandler } from "react";
import { useLocation, useNavigate } from "react-router-dom";

import HeaderComponent from "../components/header/headerComponent";
import ValidatableInput from "../components/validatableInput/validatableInputComponent";

import Ajax from "../api/ajax";
import regexp from '../base/regexp';

import store from "../redux/store";
import { AuthActions } from "../redux/reducers/authReducer";

import type { HistoryStateI } from "../components/router/routerTypes";

import '../index.scss'

const AuthPage: FC = () => {
  // с их помощью либо делаем редирект на то место, где требовалась
  // аутентификация, либо на главную
  const navigate = useNavigate();
  const location = useLocation();

  const [password, setPassword] = useState("");
  const [login, setLogin] = useState("");
  const [srvErr, setSrvErr] = useState("");
  const [validateErr, setValidateErr] = useState("");

  const handleSubmit: FormEventHandler<HTMLFormElement> = (event: FormEvent) => {
    event.preventDefault();
    Ajax.post({
      url: 'user/login/',
      body: {
        "login": login,
        "password": password,
      }
    }).then(({status, response}) => {
      if (status === Ajax.STATUS.ok) {
        store.dispatch(AuthActions.login(response.data));
        if ((location.state as HistoryStateI)?.from) {
          navigate((location.state as HistoryStateI).from);
        } else {
          navigate('/');
        }
      } else {
        console.warn(response.msg);
        setSrvErr(response.msg);
      }
    })
  }

  const handlePasswordChange: ChangeEventHandler<HTMLInputElement> = (e: FormEvent<HTMLInputElement>) => {
    setPassword(e.currentTarget.value)
  }

  const handleLoginChange: ChangeEventHandler<HTMLInputElement> = (e: FormEvent<HTMLInputElement>) => {
    setLogin(e.currentTarget.value)
  }

  return (
  <div className="auth">
    <HeaderComponent
      nameLinkMapT={{}}
      displayedNames={[]}
      nameOfActive={''} />
    <div className="container">
      <h3 className="theme-color">&nbsp;</h3>

        <div className="row">
          <div className="plate col s4 offset-s4">

            <form className="w-100" onSubmit={handleSubmit}>
              <div className="container">
                <h4 className="center">Вход</h4>
                <div className="divider"></div>
                <div className="row">
                  <div className="input-field col s12">
                    <input id="first_name" type="text" className="validate"
                      {...regexp.login.validation}
                      value={login}
                      onChange={handleLoginChange}
                    ></input>
                    <label htmlFor="first_name">Логин</label>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s12">
                    <input id="password" type="password" className="validate"
                      {...regexp.password.validation}
                      value={password}
                      onChange={handlePasswordChange}
                    ></input>
                    <label htmlFor="password">Password</label>
                    <span className="helper-text"
                      data-error={regexp.password.errorMsg}
                      data-success="right" >
                    </span>
                    {/* if (!regexp.login.test(login)) {
                      formWarning.style.display = 'block';
                      formWarningLabel.classList.add('form__warning-label-show');
                      formWarning.textContent = 'Логин - это латинские буквы, цифры и ' +
                          'нижнее подчеркивание (_). Длина логина - от 4 до 20 символов';
                      return;
                    }

                    if (!regexp.password.test(password)) {
                      formWarning.style.display = 'block';
                      formWarningLabel.classList.add('form__warning-label-show');
                      formWarning.textContent = 'В качестве пароля используйте любую ' +
                          'комбинацию непробельных символы длиной более 8';
                      return;
                    } */}
                  </div>
                </div>
                {srvErr ?
                  <div className="row">
                    <p>{srvErr}</p>
                  </div>
                : ""}
                <div className="row">
                  <button className="btn light-blue lighten-1 waves-effect waves-light right" type="submit" name="action">
                    Войти
                    <i className="material-icons right">arrow_forward</i>
                  </button>
                </div>

                {/* <div className="row">
                  <div className="input-field col s12">
                    <ValidatableInput id="test" type="text" className="validate"
                      errorConditions={[
                        {
                          errorMsg: 'длина логина меньше 4',
                          errorCondition: (input: string) => input.length < 4 
                        },
                        {
                          errorMsg: 'длина меньше 3',
                          errorCondition: (input: string) => input.length < 3 
                        }
                      ]}
                      onChange={handleTestChange}
                    />
                    <label htmlFor="test">test</label>
                  </div>
                </div> */}

              </div>
            </form>

          </div>
        </div>

      </div>
  </div>
    
)};

export default AuthPage;
