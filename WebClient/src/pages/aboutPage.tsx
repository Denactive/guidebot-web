import { FunctionComponent } from "react";

const AboutPage: FunctionComponent<{}> = () => (
  <h2>About</h2>
);

export default AboutPage;