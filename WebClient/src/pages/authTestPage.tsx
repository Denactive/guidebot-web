import { FC, useState, FormEvent, FormEventHandler, ChangeEventHandler } from "react";
import Ajax from "../api/ajax";
import HeaderComponent from "../components/header/headerComponent";
import regexp from '../base/regexp';

import store from "../redux/store";
import { AuthActions, selectAuth } from "../redux/reducers/authReducer";
import { useSelector } from "react-redux";

import '../index.scss'

const AuthTestPage: FC = () => {
  const [password, setPassword] = useState("");
  const [login, setLogin] = useState("");
  const user = useSelector(selectAuth);
  const [srvErr, setSrvErr] = useState("");
  const [validateErr, setValidateErr] = useState("");

  const handleSubmit: FormEventHandler<HTMLFormElement> = (event: FormEvent) => {
    event.preventDefault();
    Ajax.post({
      url: 'user/login/',
      body: {
        "login": login,
        "password": password,
      }
    }).then(({status, response}) => {
      if (status === Ajax.STATUS.ok) {
        store.dispatch(AuthActions.login(response.data))
      } else {
        console.warn(response.msg);
        setSrvErr(response.msg);
      }
    })
  }

  const handlePasswordChange: ChangeEventHandler<HTMLInputElement> = (e: FormEvent<HTMLInputElement>) => {
    setPassword(e.currentTarget.value)
  }

  const handleLoginChange: ChangeEventHandler<HTMLInputElement> = (e: FormEvent<HTMLInputElement>) => {
    setLogin(e.currentTarget.value)
  }

  return (
  <div className="auth">
    <HeaderComponent
      nameLinkMapT={{}}
      displayedNames={[]}
      nameOfActive={''} />
    <div className="container">
      <h3 className="theme-color">&nbsp;</h3>

        <div className="row">
          <div className="plate col s4 offset-s4">

            <form className="w-100" onSubmit={handleSubmit}>
              <div className="container">
                <h4 className="center">Вход</h4>
                <div className="divider"></div>
                <div className="row">
                  <div className="input-field col s12">
                    <input id="first_name" type="text" className="validate"
                      value={login}
                      onChange={handleLoginChange}
                    ></input>
                    <label htmlFor="first_name">Логин</label>
                  </div>
                </div>
                <div className="row">
                  <div className="input-field col s12">
                    <input id="password" type="password" className="validate"
                      value={password}
                      onChange={handlePasswordChange}
                    ></input>
                    <label htmlFor="password">Password</label>
                    <span className="helper-text"
                      data-error="wrong"
                      data-success="right" >
                    </span>
                    {/* if (!regexp.login.test(login)) {
                      formWarning.style.display = 'block';
                      formWarningLabel.classList.add('form__warning-label-show');
                      formWarning.textContent = 'Логин - это латинские буквы, цифры и ' +
                          'нижнее подчеркивание (_). Длина логина - от 4 до 20 символов';
                      return;
                    }

                    if (!regexp.password.test(password)) {
                      formWarning.style.display = 'block';
                      formWarningLabel.classList.add('form__warning-label-show');
                      formWarning.textContent = 'В качестве пароля используйте любую ' +
                          'комбинацию непробельных символы длиной более 8';
                      return;
                    } */}
                  </div>
                </div>
                {srvErr ?
                  <div className="row">
                    <div className="input-field col s12">
                      <input id="warning" type="" readOnly={true} value={srvErr}></input>
                      <label htmlFor="warning">Ошибка</label>
                    </div>
                  </div>
                : ""}
                <div className="row">
                  <button className="btn light-blue lighten-1 waves-effect waves-light right" type="submit" name="action">
                    Войти
                    <i className="material-icons right">arrow_forward</i>
                  </button>
                </div>
                <div className="row">
                  <button className="btn light-blue lighten-1 waves-effect waves-light right" type="button" name="action"
                    onClick={() => {
                      Ajax.post({
                        url: 'user/login/',
                        body: {
                          "login":"admin_junior2",
                          "password":"admin_junior2",
                        }
                      }).then(({status, response}) => {
                        if (status === Ajax.STATUS.ok) {
                          store.dispatch(AuthActions.login(response.data))
                        } else {
                          console.warn(response.msg);
                        }
                      })
                    }}
                  >
                    login
                  </button>
                </div>
                <div className="row">
                  <button className="btn light-blue lighten-1 waves-effect waves-light right" type="button" name="action"
                    onClick={() => {
                      Ajax.get({
                        url: 'user/',
                      }).then(({status, response}) => {
                          if (status === Ajax.STATUS.ok) {
                            store.dispatch(AuthActions.update(response.data))
                          } else {
                            console.warn(response.msg);
                          }
                        }
                      )
                    }}
                  >
                    user
                  </button>
                </div>
                <div className="row">
                  <button className="btn light-blue lighten-1 waves-effect waves-light right" type="button" name="action"
                    onClick={() => {
                      if (user.email === "") {
                        console.warn('get user first');
                        return
                      }
                      const name = user.email.endsWith('22@admin.us') ? 'admin_junior2' : 'admin_junior22'
                      Ajax.put({
                        url: 'user/update/',
                        body: {
                          // проверил, эти 3 поля могут быть пустыми или отсутствовать - ничего не меняется
                          firstName: `${name.endsWith('22') ? name.toUpperCase() : 'John'}`,
                          lastName: `${name.endsWith('22') ? name.toUpperCase() : 'Dow'}`,
                          email: `${name}@admin.us`,

                          // login: name, // проверил. Попытка изменения этого поля игнорируется 
                          password: name, // проверил. Пароль меняется.
                        }
                      }).then(({status, response}) => {
                          if (status === Ajax.STATUS.ok) {
                            store.dispatch(AuthActions.update(response.data))
                          } else {
                            console.warn(response.msg);
                          }
                        }
                      )
                    }}
                  >
                    update
                  </button>
                </div>
                <div className="row">
                  <button className="btn light-blue lighten-1 waves-effect waves-light right" type="button" name="action"
                    onClick={() => {
                      Ajax.post({
                        url: 'logout/',
                      })
                    }}
                  >
                    logout
                  </button>
                </div>
              </div>
            </form>

          </div>
        </div>

      </div>
  </div>
    
)};

export default AuthTestPage;
