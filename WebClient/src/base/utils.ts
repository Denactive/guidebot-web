import {Temporal} from '@js-temporal/polyfill';

export function timeISORus() {
  return Temporal.Now.plainTimeISO().toLocaleString();
}
