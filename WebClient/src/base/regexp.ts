type InputHTML5ValidationAttributesT = {
  pattern: RegExp;
  errorMsg: string;
  validation: {
    minLength?: number;
    maxLength?: number;
    required?: boolean;
    type?: any;
  };
};

// Только буквенные латинские символы и _ числом от 4 до 256
export const login = /^[a-zA-Z0-9_]{4,256}$/;

// Любые символы числом от 8 до 128
export const password = /^\S{8,128}$/;

// Имена не короче 2-х букв.
export const firstName = /^[a-zA-Zа-яА-ЯЁё]{2,256}$/;

// Поддерживает двойные фамилии, апострофы, дефисы. от 2 до 256
export const lastName =
  /^[a-zA-Zа-яА-Я]{1,}'?-?[a-zA-Zа-яА-Я]{1,}(\s[a-zA-Zа-яА-Я]{1,256})?$/;

// Аналог [а-яёА-ЯЁ]
export const cirillic = /[\u0401\u0451\u0410-\u044f]/;

export const regexp = {
  login,
  password,
  firstName,
  lastName,
  cirillic,
};

export const regexpValidation: {
  [key: string]: InputHTML5ValidationAttributesT;
} = {
  login: {
    pattern: login,
    errorMsg: 'Логин - это латинские буквы, цифры и ' +
      'нижнее подчеркивание (_). Длина логина - от 4 символов',
    validation: {
      minLength: 4,
      maxLength: 256,
      required: true,
    },
  },
  password: {
    pattern: password,
    errorMsg: 'Любая комбинация непробельных символов длиной более 8',
    validation: {
      minLength: 8,
      maxLength: 128,
      required: true,
    },
  },
  firstName: {
    pattern: firstName,
    errorMsg: '',
    validation: {
      minLength: 2,
      maxLength: 256,
    },
  },
  lastName: {
    pattern: lastName,
    errorMsg: '',
    validation: {
      minLength: 2,
      maxLength: 256,
    },
  },
};

export default regexpValidation;
