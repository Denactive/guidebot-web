# GuideBot
The thesis project of Bauman Moscow State University

**[На русском](README.md)**

# Description #

# Documentation #
- [Documentation](https://drive.google.com/drive/folders/1zevx_SmwngV8QqQkeFI_AQbh50U1BMo9?usp=sharing)

# Project manager #
- [Kanev Antony](http://iu5.bmstu.ru/user/profile.php?id=2271)

# Developers #
- [Denis Kirillov](https://vk.com/denactive)

# Installation an start #

### Development launch mode
**dev-server + Django**

**One button launch script (it is needed to be executed in the root folder)**

Windows 10 one button launch script
  1. ```npm i```
  2. ```npm start```

Linux Ubuntu 20.04 one button launch script
  1. ```sudo apt install tmux```
  2. ```sudo npm i```
  3. ```sudo npm start-linux```

**Admin panel**
``` http://localhost:8000/admin/ ```
Default superuser record: Login: ``` admin ```, Password: ``` admin ```

### Server app
**Supported operating systems**

- Windows 10 / Ubuntu 20.04

**Software**

- python 3.9
- python libraries from [requirements.txt](./ServerPy/requirements.txt)

**Installation**

1. ```cd ServerPy```
2. ```sudo apt-get install python3.9```
3. __Unnecessary__ A Linux Python interpreter called with __python3__, A Windows one - __py__.

    ```sudo alias py=python3```
4. __Unnecessary__ You may not want to use a virtual environment. If so, skip to 6.

    ```sudo apt-get install python3.9-venv```
5. ```sudo py -m venv venv```
6. Windows: ```./venv/Scripts/activate```
    
    Linux: ```source venv/bin/activate```
7. ```sudo py -m pip install -r requirements```
8. ```sudo py manage.py collectstatic``` 
9. ```sudo py manage.py runserver 8080``` 

---

### Web-client app
**Supported operating systems**

- Windows 10 / Ubuntu 20.04

**Software**

- npm 6.14.11+
- node 14.16.0+
- js libraries from [package.json](./WebClient/package.json)

**Installation**

1. ```cd WebClient```
2. ```sudo curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh```
3. ```sudo bash nodesource_setup.sh``` - update Node
4. ```sudo rm nodesource_setup.sh``` - delete a Node installation script
5. ```sudo apt-get install nodejs npm```
6. ```sudo npm install -g npm@latest```
7. ```node -v``` - check if Node is now 14.16.0+
8. ```sudo npm i```
9. ```sudo npm start```

---

# License #
[GNU GPLv2 License](./LICENSE)
